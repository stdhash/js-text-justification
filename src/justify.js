function justify(text, length) {
	let lines = calculateLines(text.split(' '), length);
	return lines.map(line => justifyLine(line, length)).join('\n');
}

function calculateLines(words, length, lines = [], line = [], count = 0) {
	if (!words.length) {
		return lines.concat([line]);
	}

	let nextWord = words[0];
	let wordsLeft = words.slice(1);
	let extraChars = nextWord.length + (line.length ? 1 : 0);
	let nextCount = count + extraChars;

	if (nextCount > length) {
		return calculateLines(words, length, lines.concat([line]), [], 0);
	}

	return calculateLines(wordsLeft, length, lines, line.concat(nextWord), nextCount);
}

function justifyLine(words, length) {
	if (words.length === 1) {
		return words[0] + ' '.repeat(length - words[0].length);
	}

	let numOfChars = words.reduce((count, word) => count + word.length, 0);
	let numOfExtra = length - numOfChars;
	let numOfPads = words.length - 1;
	let numOfSpaces = Math.floor(numOfExtra / numOfPads);
	let spaces = ' '.repeat(numOfSpaces);
	let extraSpaces = numOfExtra - numOfSpaces * numOfPads;
	let paddedWords = words.slice(1).map((word, i) => {
		let additionalSpace = '';

		if (i < extraSpaces) {
			additionalSpace = ' ';
		}

		return spaces + additionalSpace + word;
	});

	return [words[0], ...paddedWords].join('');
}
